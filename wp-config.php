<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Apollo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e74#)e-Txs1FzTJKO{_fcR.6o<y(N|cNqHy[)>3}=EfMg !Q1jo>UQd:=<Ub>wGE');
define('SECURE_AUTH_KEY',  '5-._J>8/fUsKk5oF*&xl;H;S? *k;3p4A]zX1Q`4hhzV@i>NNVLAj:hb-h@uu]cX');
define('LOGGED_IN_KEY',    'o3||-r3?_$C&#k<=qM=;O@D:}Qt,rS/R^SXLaCt>m|lD&eJ+A;b@`M{X %>#K>W&');
define('NONCE_KEY',        'kQv+Nu4l[$70<.k4&+?JSXR>3<LXs]Na>Pg&VB`pB ^fNkB J9X,}mX Iu5%@_9?');
define('AUTH_SALT',        'u6hqd]&d ljzc>=f|3Ejnw.;`O$r|nwzx8d[8RRc`u.~RWQ B4aj;$M{Z/sf>C*z');
define('SECURE_AUTH_SALT', 'KrmBO|D1+)wh`0:rA;Lz-`gFiA%C8xjn-p;8xizCK*tb:A@_fxDeiF{l@s=l(U:z');
define('LOGGED_IN_SALT',   'Yq</!$k{CMgRvji,g#$:BV;N}@J(bnpT5l!`br*Kt?v*h2+D=.jq 8]W$W2.MtLW');
define('NONCE_SALT',       '~Yr,WMt&#seII{&pt4N(:-G:H;~9j!bN^%wY]6%FDz>D~^GCzhkqneMoCvm4d^@N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG',               true);
define('WP_POST_REVISIONS',      3);
define('FS_METHOD', 			'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
