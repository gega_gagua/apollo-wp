<?php
/**
 * GegaGagua functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GegaGagua
 *




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/init.php';


/**
 * Register widgets
 */
require get_template_directory() . '/inc/register.php';


/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/assets.php';


