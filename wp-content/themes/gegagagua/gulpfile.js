var gulp      = require('gulp'),
  sass    = require('gulp-sass'),
  watch     = require('gulp-watch'),
  jshint    = require('gulp-jshint'),
  gulpif    = require('gulp-if'),
  uglify    = require('gulp-uglify'),
  cssnano   = require('gulp-cssnano'),
  rename    = require("gulp-rename"),
  concatCss   = require('gulp-concat-css'), 
  browserSync = require('browser-sync'),
  sourcemaps  = require('gulp-sourcemaps'),
  imagemin  = require('gulp-imagemin');



/*
* BrowserSync
*/
gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "localhost/wi-wp"
    });
});




/*
* Js Minification
*/
var condition = true; // TODO: add business logic
gulp.task('jsminify', function() {
    
    return  gulp.src('assets/src/js/*.js')
        .pipe(gulpif(condition, uglify()))
        .pipe(watch('assets/src/js/**/*.js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'));

});




/*
* Css Minification
*/
gulp.task('cssminify', function() {
    
  var all_css = gulp.src('assets/src/css/*.css')
        // .pipe(concatCss("main.css"))
    ;
    return  all_css
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css'));

});




/*
* Jshint reporter to show JS errors
*/
gulp.task("lint", function() {
    gulp.src("assets/src/js/*.js")
        .pipe(jshint())
        .pipe(jshint.reporter("default"));
});




/*
* Images minification
*/
gulp.task('imagesminify', function(){
    
    return  gulp.src('assets/src/images/**/*')
      .pipe(imagemin({
              progressive: true
          }))
      .pipe(gulp.dest('assets/images'))

});



/*
* Sass to minify css
*/
gulp.task('sass', function(){
    return gulp.src('assets/src/css/*.scss')
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe( watch('assets/src/css/*.css') )
    .pipe(rename({
          suffix: '.min'
      }))
      .pipe(sass())
      .pipe(cssnano())
      .pipe(browserSync.reload({
          stream: true
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('assets/css'));
});




/*
* Fonts to fonts directory
*/
gulp.task('fonts', function() {
    return  gulp.src('assets/src/fonts/**/*')
        .pipe(gulp.dest('assets/fonts'))
})



// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch( 'assets/src/css/**/*.css' , ['cssminify']);
});



/*
* Run functions
*/
gulp.task('default', ['watch', 'jsminify', 'cssminify', 'fonts'], function (){
  // ... watchers
})
