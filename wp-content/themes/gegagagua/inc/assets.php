<?php 
function gegagagua_scripts() {
	
	wp_enqueue_style( 'gegagagua-style', get_stylesheet_uri() );

	wp_enqueue_style( 'base', get_template_directory_uri() . '/assets/css/base.min.css' );
	wp_enqueue_style( 'colorbox', get_template_directory_uri() . '/assets/css/colorbox.min.css' );
	wp_enqueue_style( 'layout', get_template_directory_uri() . '/assets/css/layout.min.css' );
	wp_enqueue_style( 'skeleton', get_template_directory_uri() . '/assets/css/skeleton.min.css' );



	// wp_enqueue_script( 'gegagagua-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

}
add_action( 'wp_enqueue_scripts', 'gegagagua_scripts' );