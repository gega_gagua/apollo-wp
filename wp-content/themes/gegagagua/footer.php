<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GegaGagua
 */

?>

	
	
	<div id="copy">
		<div class="container">
		<section class="cbp-so-section">
			<div class="sixteen columns cbp-so-side cbp-so-side-left">
				<p>© 2013 Apollo. All rights reserved. Theme by IG DESIGN.</p>
			</div>
			<div class="sixteen columns cbp-so-side cbp-so-side-right">
				<div class="cl-effect-18">
					<a href="#" data-hover="facebook">facebook</a>
					<a href="#" data-hover="Twitter">Twitter</a>
					<a href="#" data-hover="Google+">Google+</a>
					<a href="#" data-hover="Linkedin">Linkedin</a>
					<a href="#" data-hover="Instagram">Instagram</a>
				</div>
			</div>
		</section>	
		</div>	
	</div>		
	
	
	
	
	

	
</div>		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
<!-- JAVASCRIPT
================================================== -->
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/modernizr.custom.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.nicescroll.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.sticky.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.parallax-1.1.3.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.localscroll-1.2.7-min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.scrollTo-1.4.2-min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/classie.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/cbpScroller.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.maximage.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.knob.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.bxslider.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.easing.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.typer.html"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.isotope.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.masonry.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.colorbox.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/owl.carousel.min.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/template.min.js"></script>  	  
<!-- End Document
================================================== -->


<?php wp_footer(); ?>

</body>
</html>
