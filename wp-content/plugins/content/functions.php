<?php 	

/**
 *  Relationships start
 */
//relationship get id and title  
global $post;
function relationship($post_type)
{
    $args = array(
        'posts_per_page'   => -1,
        'post_type'        => $post_type,
        'post_status'      => 'publish',
        'suppress_filters' => true 
    );
    $posts = get_posts( $args );

    $result = array('0'=>'Choose one');
    foreach ($posts as $key => $post) {
        $result[$post->ID] = $post->post_title;
    }

    return $result;
}


function hotel_relationship()
{
    // return $result;
    return relationship('hotel');
}


function all_data(){
	$args = array(
        'posts_per_page'   => -1,
        'post_type'        => array('post','tour','page'),
        'post_status'      => 'publish',
        'suppress_filters' => true 
    );
    $posts = get_posts( $args );

    $result = array('0'=>'Choose one');
    foreach ($posts as $key => $post) {
        $result[$post->ID] = $post->post_title;
    }

    return $result;
}

