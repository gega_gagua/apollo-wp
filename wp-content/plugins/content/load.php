<?php

/*
Plugin Name: Content
Description: Custom Post Types, Taxonomies and Custom fields. 
Author: Webintelligence
Version: 1.0
Author URI: http://webintelligence.de/
*/


/**
 *  Load cmb2
 */
if(file_exists(plugin_dir_path(__FILE__).'../cmb2/init.php')) 
require_once(plugin_dir_path(__FILE__).'../cmb2/init.php');



/**
 *  Load function.php
 */
if(file_exists(plugin_dir_path(__FILE__).'../content/functions.php')) 
require_once(plugin_dir_path(__FILE__).'../content/functions.php');



/**
 *  Load select2
 */
if(file_exists(plugin_dir_path(__FILE__).'../content/inc/cmb-field-select2/cmb-field-select2.php')) 
require_once(plugin_dir_path(__FILE__).'../content/inc/cmb-field-select2/cmb-field-select2.php');


/**
 *  Load custom relationship field
 */
if ( ! function_exists( 'cmb2_attached_posts_fields_render' ) ) {
	require_once WP_PLUGIN_DIR . '/content/inc/cmb2-attached-posts/cmb2-attached-posts-field.php';
}


/**
 *  Load custom search field
 */
if(file_exists(plugin_dir_path(__FILE__).'../content/inc/CMB2-Post-Search-field/cmb2_post_search_field.php')) 
require_once(plugin_dir_path(__FILE__).'../content/inc/CMB2-Post-Search-field/cmb2_post_search_field.php');







$files = glob(dirname(__FILE__).'/custom-post-types/*.php');

foreach($files as $file)
{
	require_once $file;

}


/**
 *  Enqueue admin scripts and styles.
 */
function content_admin_assets()
{
$plugin_url = plugins_url() . '/content/';
	/** 
	 *  LOAD: stylesheet for admin
	 */	
	wp_enqueue_style('admin-content-css', $plugin_url .'/assets/css/admin-cmb2.css', array(), '', false);


	/* 
	*  LOAD: javascript for admin
	*/		
	 wp_enqueue_script('admin-content-js',  $plugin_url .'/assets/js/admin-cmb2.js', array(), '', false);

}

add_action('admin_enqueue_scripts', 'content_admin_assets');







