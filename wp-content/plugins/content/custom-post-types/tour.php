<?php

function codex_custom_init_tour() {
	$tour_labels = array(
		'name'               => _x( 'Tour', 'post type general name'),
		'singular_name'      => _x( 'Tour', 'post type singular name'),
		'menu_name'          => _x( 'Tour', 'admin menu'),
		'name_admin_bar'     => _x( 'Tour', 'add new on admin bar'),
		'add_new'            => _x( 'Add New', 'cmb2'),
		'add_new_item'       => __( 'Add New Tour'),
		'new_item'           => __( 'New Tour'),
		'edit_item'          => __( 'Edit Tour'),
		'view_item'          => __( 'View Tour'),
		'all_items'          => __( 'All Tours'),
		'search_items'       => __( 'Search Tour'),
		'parent_item_colon'  => __( 'Parent Tour:'),
		'not_found'          => __( 'No Tour found.'),
		'not_found_in_trash' => __( 'No Tour found in Trash.')
	);

     $args = array( 
      'public' => true, 
      'labels'      => $tour_labels,
      'menu_icon'	=> 'dashicons-groups',
      'supports'	=> array( 'thumbnail', 'editor', 'title' , 'page-attributes' )  
    ); 
    register_post_type( 'tour', $args );
 } 
 add_action( 'init', 'codex_custom_init_tour' );


add_filter( 'cmb2_meta_boxes', 'cmb2_add_metabox_tour' );
function cmb2_add_metabox_tour( ) {

	//tour_details
	 $tour_details = new_cmb2_box( array(
	    'id'           => 'tour_details',
	    'title'        => 'tour Details',
	    'object_types' => array( 'tour' ), // post type
	    'context'      => 'normal', //  'normal', 'advanced', or 'side'
	    'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	    'show_names'   => true, // Show field names on the left
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Price', 'content' ),
		"id"      =>  "tour_price",
		"type"    => "text",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Place', 'content' ),
		"id"      =>  "tour_place",
		"type"    => "text",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Description Word', 'content' ),
		"id"      =>  "desc",
		"type"    => "text",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Date', 'content' ),
		"id"      =>  "date",
		"type"    => "text",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Type', 'content' ),
		"id"      =>  "type",
		"type"    => "text",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Cooming Soon', 'content' ),
		"id"      =>  "cooming_soon",
		"type"    => "checkbox",
	) );

	$tour_details->add_field(array(
	    "name"    => __( 'Gallery', 'content' ),
		"id"      =>  "gallery",
		"type"    => "file_list",
	) );

}