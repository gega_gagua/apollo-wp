<?php 
add_filter( 'cmb2_meta_boxes', 'cmb2_add_metabox_post' );
function cmb2_add_metabox_post( ) {


	//post_group
	 $post_group = new_cmb2_box( array(
	    'id'           => 'post_group',
	    'title'        => 'post_Group Details',
	    'object_types' => array( 'post' ), // post type
	    // 'show_on'      => array( 'key' => 'page-template', 'value' => 'tpl-post.php' ),
	    'context'      => 'normal', //  'normal', 'advanced', or 'side'
	    'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	    'show_names'   => true, // Show field names on the left
	) );


	$post_group->add_field(array(
	    "name"    => __( 'Place', 'content' ),
		"id"      =>  "place",
		"type"    => "text",
	) );

}